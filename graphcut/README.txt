ANKUSH ROY
CCID: ankush2
CMPUT 617
=====================================================================================

The file main.m contains the code for interactive segmentation of an object from its background.
Put main.m, the image C_Gems.png in the  folder containing the matlab_bgl toobox 
Run main.m to set the interactive segmentation of Foreground and Background

The folder also contains a write up explaining the different steps involved in the segmentation
based on Yuri Boykovs paper.

The approach also shows that the larger the area of learning the better the results are.

