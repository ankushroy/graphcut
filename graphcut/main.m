% main
function main();
clear;
clc;
addpath '/home/ankush/Folder/CMPUT 617/Assignment'/matlab_bgl.u1conflict'/';

a=imread('Ankush.jpg');
a= rgb2gray(a);
b=im2double(a);
[M_act,N_act]=size(b);
KKL=b;
b=medfilt2(b); %%% Median Filter to remove salt and pepper noise or impulse noise
cc=histeq(b);
b=abs(cc); 
[M,N]= size(b);
b=b/max(b(:));
c=b;
d=b;
disp('Waiting for user input')
figure(1),imshow(KKL);title('Select End Points of rectangles in the image Region and in Background')
[cx,cy] = ginput(4);
cx = round(cx); cy = round(cy);
if cx(2,1)<cx(1,1)
    Exc = cx(2,1);
    cx (2,1) = cx (1,1);
    cx (1,1) = Exc;
end

if cy(2,1)<cy(1,1)
    Exc = cy(2,1);
    cy (2,1) = cy (1,1);
    cy (1,1) = Exc;
end

if cx(4,1)<cx(3,1)
    Exc = cx(4,1);
    cx (4,1) = cx (3,1);
    cx (3,1) = Exc;
end

if cy(4,1)<cy(3,1)
    Exc = cy(4,1);
    cy (4,1) = cy (3,1);
    cy (3,1) = Exc;
end

for i=cx(1,1):cx(2,1)
    for j=cy(1,1):cy(2,1)
        d(i,j)=4;
    end
end

for i=cx(3,1):cx(4,1)
    for j=cy(3,1):cy(4,1)
        d(i,j)=5;
    end
end
disp('User Input Received')
figure(1),imshow(KKL)
hold on
rectangle('Position',[cx(1,1),cy(1,1),cx(2,1)-cx(1,1),cy(2,1)-cy(1,1)],'FaceColor','r')
rectangle('Position',[cx(3,1),cy(3,1),cx(4,1)-cx(3,1),cy(4,1)-cy(3,1)],'FaceColor','b')
title('Selected rectangles: RED -> Foreground,  BLUE -> Background')

Image_Region = imcrop(b,[cx(1,1)  cy(1,1) cx(2,1)-cx(1,1) cy(2,1)-cy(1,1)]);
Image_Background = imcrop(b,[cx(3,1)  cy(3,1) cx(4,1)-cx(3,1) cy(4,1)-cy(3,1)]);
disp('Learning Histogram Model')

%%%%% Approximating Histogram to 10 Bins   %%%%%%%%%%%%%%%%%%%%%
%%%%% Image Region Learning %%%%%%%%%%%

[Raw_Region,nn] = imhist(Image_Region);
[Raw_Background,nn1] = imhist(Image_Background);
k=1;
for i=1:5:256
    if (i ~=256)
     Region_Binned(k,1) = sum(Raw_Region(i:i+1,1))/2;
     Region_Background(k,1) = sum(Raw_Background(i:i+1,1))/2;
     k=k+1;
     end
end
B=ones(M*N+2,1);
for i=1:M*N
B(i+1,1) = b(i);
end
sigma = 5;
disp('Assigning Value to the user defined hard Constraints')
Raw_Region = sum(Region_Binned(:));
Raw_Background =  sum(Region_Background(:));
Normalised_Object = Region_Binned/Raw_Region;
Normalised_Background = Region_Background/Raw_Background;
Source=zeros(M*N+2,1);
Sink = zeros(M*N+2,1);
JJ = 40;

disp('Building the edge matrix')
KLKL=M*N;
E = edges4connected(M,N);
E1=[ones(M*N+2,1);E(:,1);(M*N+2)*ones(M*N+2,1);(2:M*N+1)';(2:M*N+1)'];
E2=[(1:M*N+2)';E(:,2);(1:M*N+2)';ones(M*N,1);(M*N+2)*ones(M*N,1)];
V = 100*exp(-abs(b(E(:,1))-b(E(:,2))))./(2*sigma^2);
K= 1 + sum(V(:));
for i=1:M*N

                Source(i+1,1) = -JJ*log(Normalised_Background(ceil(B(i)*255/5+eps),1)+eps);
                Sink(i+1,1)   =  -JJ*log(Normalised_Object(ceil(B(i)*255/5+eps),1)+eps);
end
V1=[ceil(Source);ceil(V);ceil(Sink);ceil(Source(2:M*N+1));ceil(Sink(2: M*N+1))];
A = sparse(E1,E2,V1,KLKL+2,KLKL+2);
clear B;
clear a;
clear Source;
clear Sink;
disp('calculating max flow min cut')        
[flowval cut] = max_flow(A,1,M*N+2);
CUT(cut==-1 )=0;
CUT(cut==1)=1;
disp('Final Segmented Image')
Kl = reshape(CUT(2:M*N+1),M,N);
Output = KKL.*Kl;
Output(Output==0) =255;
figure(2),imshow(Output)
title('Segmented Image')
 end
          
     %%%%%%%%% FUNCTION TO CREATE THE PAIRWISE EDGE CO ORDINATES %%%%%%%%%%%%%%%
function E = edges4connected(height,width)
N = height*width;
I = []; J = [];
is = [1:N]'; is([height:height:N])=[];
js = is+1;
I = [I;is;js];
J = [J;js;is];
is = [1:N-height]';
js = is+height;
I = [I;is;js];
J = [J;js;is];
E = [I,J];
end
